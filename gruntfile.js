
module.exports = function(grunt) {
	function gitignorePatterns() {
		var patterns = grunt.file.read('.gitignore').replace(/\r/g, '\n').split('\n');

		var converted = [];
		for (var i = 0; i < patterns.length; i++) {
			var pattern = patterns[i].trim();

			var commentStart = pattern.indexOf('#');
			if (commentStart >= 0)
				pattern = pattern.substring(0, commentStart).trim();

			if (!pattern)
				continue;

			if (pattern.charAt(0) == '!')
				continue;

			if (pattern.charAt(0) == '/')
				pattern = pattern.substring(1);

			pattern = '!' + pattern;

			if (pattern.charAt(pattern.length-1) == '/')
				pattern = pattern + '**';

			converted.push(pattern);
		}

		return converted;
	}

	var everything = ['**/*'];
	var exceptSources = ['!**/*.ts', '!**/*.less', '!**/*.js.map', '!**/*.css.map'];
	var exceptIgnored = gitignorePatterns();
	var exceptNodeProject = ["!package.json", "!gruntfile.js"];
	var sourcesAndResources = [].concat(everything, exceptIgnored, exceptNodeProject);
	var resourcesOnly = [].concat(everything, exceptIgnored, exceptNodeProject, exceptSources);

	var buildFolder = 'build/';
	var typescriptOutput = buildFolder + 'res/script.js';
	var lessOutputs = {
		'build/res/stylesheet.css': 'res/style.less'
	};
	var uglifyOutputs = {
		'build/res/script.js': ['build/res/script.js']
	};

	var typescriptSources = ['**/*.ts'].concat(exceptIgnored, exceptNodeProject);
	var lessSources = ['**/*.less'].concat(exceptIgnored, exceptNodeProject);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		copy: {
			development: {
				files: [{
					expand: true,
					cwd: '',
					src: sourcesAndResources,
					dest: buildFolder
				}]
			},
			production: {
				files: [{
					expand: true,
					cwd: '',
					filter: 'isFile',
					src: resourcesOnly,
					dest: buildFolder
				}]
			}
		},

		clean: {
			src: [buildFolder]
		},

		ts: {
			development: {
				src: typescriptSources,
				dest: typescriptOutput,
				options: {
					fast: 'never'
				}
			},
			production: {
				src: typescriptSources,
				dest: typescriptOutput,
				options: {
					sourceMap: false,
					fast: 'never'
				}
			}
		},

		less: {
			development: {
				options: {
					sourceMap: true,
					sourceMapFileInline: true,
					sourceMapRootpath: '/',
					paths: ['.']
				},
				files: lessOutputs
			},
			production: {
				options: {
					compress: true,
					optimization: 0,
					paths: ['.']
				},
				files: lessOutputs
			}
		},

		uglify: {
			production: {
		        files: uglifyOutputs
		    }
		},

		watch: {
			ts: {
				files: typescriptSources,
				tasks: ['ts:development'],
				options: {
					spawn: false
				}
			},
			less: {
				files: lessSources,
				tasks: ['less:development'],
				options: {
					spawn: false
				}
			},
			resources: {
				files: sourcesAndResources,
				tasks: ['diffCopy:development'],
				options: {
					event: ['added', 'changed', 'renamed'],
				}
			}
		},

		concurrent: {
			development: {
				tasks: ['connect:development', 'watch:resources', 'watch:less', 'watch:ts'],
				options: {
					logConcurrentOutput: true
				}
			}
		},

		connect: {
			development: {
				port: 8080,
				base: buildFolder
			}
		}
	});

	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-connect');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-diff-copy');
	grunt.loadNpmTasks('grunt-ts');

	grunt.registerTask('development', ['clean', 'diffCopy:development', 'less:development', 'ts:development', 'concurrent:development']);
	grunt.registerTask('production', ['clean', 'diffCopy:production', 'less:production', 'ts:production', 'uglify:production']);

	grunt.registerTask('default', ['development']);
};
