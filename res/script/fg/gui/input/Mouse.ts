/// <reference path="../../../defs/jquery/jquery.d.ts" />
/// <reference path="../../core/StringDictionary.ts" />
/// <reference path="../../core/EventProperty.ts" />

module fg.gui.input {

    /** A button of the mouse. */
    export enum MouseButton {
        left = 1,
        middle = 2,
        right = 3
    }

    class MouseButtonStatus {

        /** Indicates if this mouse button is currently being pressed. */
        public isDown = core.property(false);
    }

    /** Always keeps track of mouse conditions. */
    export class Mouse {

        /** The mouse position relative to the left edge of the document. */
        public pageX: number;

        /** The mouse position relative to the top edge of the document. */
        public pageY: number;

        /** The mouse horizontal coordinate within the client area (scroll ignored). */
        public clientX: number;

        /** The mouse vertical coordinate within the client area (scroll ignored). */
        public clientY: number;

        private mouseButtons: core.StringDictionary<MouseButtonStatus> = {};

        /** Get a mouse button. */
        public getButton(button: MouseButton): MouseButtonStatus {
            if (!this.mouseButtons.hasOwnProperty('btn_' + button))
                return this.mouseButtons['btn_' + button] = new MouseButtonStatus();

            return this.mouseButtons['btn_' + button];
        }

        constructor() {
            $(document).mousemove(e => {
                this.pageX = e.pageX;
                this.pageY = e.pageY;
                this.clientX = e.clientX;
                this.clientY = e.clientY;
            });

            $(document).mousedown(e => {
                this.getButton(e.which).isDown.set(true);
            });

            $(document).mouseup(e => {
                this.getButton(e.which).isDown.set(false);
            });
        }
    }
}
