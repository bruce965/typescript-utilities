/// <reference path="input/Mouse.ts" />

module fg.gui.input {

    /** Always keeps track of mouse conditions. */
    export var mouse = new Mouse();
}
