/// <reference path="../../defs/jquery/jquery.d.ts" />
/// <reference path="../core/Event.ts" />

module fg.res {

    var doNothing = () => { };

    /** Interface to Google WebFonts downloader. */
    export class WebFonts {

        private static included = false;

        private queue: [string, () => void][] = [];
        private scriptLoaded = false;

        constructor() {
            if (!WebFonts.included) {
                WebFonts.included = true;

                $.ajax({
                    type: 'GET',
                    url: "//ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js",
                    success: () => { this.scriptLoaded = true; this.dequeueFonts(); },
                    error: () => console.warn("Unable to load Google WebFonts API."),
                    dataType: "script",
                    cache: true
                });
            } else {
                this.scriptLoaded = true;
            }
        }

        /** Load a font. */
        public loadFont(family: string, weights?: number[], sets?: WebFonts.CharacterSet[]): core.Event<() => void> {
            var evnt = core.event();
            this.enqueueFont(family, weights, sets, evnt);
            this.dequeueFonts();
            return evnt;
        }

        /** Load multiple fonts at once. */
        public loadFonts(fonts: WebFonts.Font[]): core.Event<() => void> {
            var evnt = core.event();
            for (var i = 0; i < fonts.length; i++)
                this.enqueueFont(fonts[i].family, fonts[i].weights, fonts[i].sets, i == 0 ? evnt : null);
            this.dequeueFonts();
            return evnt;
        }

        private dequeueFonts(): void {
            if (!this.scriptLoaded)
                return;

            //console.debug("dequeueFonts", this.loadFonts);

            var allFamilies: string[] = [];
            var allCallbacks: (() => void)[] = [];
            for (var i = 0; i < this.queue.length; i++) {
                var fontFamily = this.queue[i][0];
                var callback = this.queue[i][1];

                allFamilies.push(fontFamily);
                allCallbacks.push(callback);
            }

            this.queue = [];

            ((allFamilies, allCallbacks) => {
                window['WebFont'].load({
                    google: {
                        families: allFamilies
                    },
                    active: () => {
                        for (var i = 0; i < allCallbacks.length; i++) {
                            try {
                                allCallbacks[i]();
                            } catch (e) {
                                console.error(e);
                            }
                        }
                    }
                });
            })(allFamilies, allCallbacks);
        }

        public enqueueFont(family: string, weights: number[], sets: WebFonts.CharacterSet[], onLoad?: core.Event<() => void>): void {
            if (!weights) weights = [400];
            if (!sets) sets = [WebFonts.CharacterSet.latin];

            var setNames: string[] = [];
            for (var i = 0; i < sets.length; i++)
                setNames.push(this.getCharacterSetName(sets[i]));

            this.queue.push([
                family.replace(/ /g, '+') + ':' + weights.join(',') + ':' + setNames.join(','),
                onLoad ? () => onLoad.fire() : doNothing
            ]);
        }

        private getCharacterSetName(set: WebFonts.CharacterSet): string {
            var techName = WebFonts.CharacterSet[set];
            var extended = 'Extended';
            if (techName.substr(techName.length - extended.length, extended.length) === extended)
                return techName.substr(0, techName.length - extended.length) + '-ext';

            return techName;
        }
    }

    export module WebFonts {

        export enum CharacterSet {
            cyrillic,
            cyrillicExtended,
            greekExtended,
            greek,
            latin,
            latinExtended,
            vietnamese
        }

        export interface Font {
            family: string;
            weights?: number[];
            sets?: CharacterSet[];
        }
    }
}
