
module fg.core {

    /** A property with a getter and a setter. */
    export interface Property<T> {

        /** Get the value of this property. */
        get(): T;

        /** Change the value of this property. */
        set(value: T): void;
    }
}
