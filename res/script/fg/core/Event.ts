
module fg.core {

	/** Create a simple event. */
	export function event(emitter?: any): Event<() => void>;
	/** Create an event with the specified signature. */
	export function event<T extends Function>(emitter?: any): Event<T>;
	export function event<T extends Function>(emitter?: any): Event<T> {
		return new Event<T>(emitter);
	}

	/** An event. */
	export class Event<T extends Function> {

		private handlers: T[] = [];

		constructor(private emitter: any = null) { }

		/** Fire this event. */
		public fire(...args: any[]): void;
		fire(): void {
			for (var k in this.handlers) {
				try {
					this.handlers[k].apply(this.emitter, arguments);
				} catch (e) {
					console.error(e);
				}
			}
		}

		/** Listen for this event with the specified function. */
		public bind(handler: T): void {
			this.handlers.push(handler);
		}

		/** Stop listening for this event with the specified function. */
		public unbind(handler: T): boolean {
			for (var k in this.handlers) {
				if (this.handlers[k] === handler) {
					this.handlers.splice(k, 1);
					return true;
				}
			}

			return false;
		}

		/** Get the number of handlers currently bound to this event. */
		public getHandlersCount(): number {
			return this.handlers.length;
		}
	}
}
