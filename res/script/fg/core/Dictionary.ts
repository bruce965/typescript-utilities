/// <reference path="StringDictionary.ts" />
/// <reference path="../util/valueToString.ts" />

module fg.core {

    /** A key-value dictionary. */
    export class Dictionary<K, V> {

        private dic: StringDictionary<{ k: K; v: V; }[]> = {};
        private valuesCount = 0;

        /** Get a value by key, returns null if key is not found. */
        public get(key: K): V {
            var keyHash = this.hash(key);
            var kvp = this.getLowLevel(key, keyHash);

            if (!kvp)
                return null;

            return kvp.v;
        }

        /** Set a value to a key, returns false if that key already has a value. */
        public set(key: K, value: V): boolean {
            if (value === undefined)
                this.remove(key);

            var keyHash = this.hash(key);
            var valuesWithThisHash = this.dic[keyHash];

            if (!valuesWithThisHash) {
                this.dic[keyHash] = [{ k: key, v: value }];
                this.valuesCount++;
                return true;
            }

            for (var i = 0; i < valuesWithThisHash.length; i++)
                if (this.areEqual(valuesWithThisHash[i].k, key))
                    return false;

            valuesWithThisHash.push({ k: key, v: value });
            this.valuesCount++;
            return true;
        }

        /** Remove a value from the dictionary, returns false if there is no value with that key. */
        public remove(key: K): boolean {
            var keyHash = this.hash(key);
            var valuesWithThisHash = this.dic[keyHash];

            if (!valuesWithThisHash)
                return false;

            for (var i = 0; i < valuesWithThisHash.length; i++) {
                if (this.areEqual(valuesWithThisHash[i].k, key)) {
                    valuesWithThisHash.splice(i, 1);

                    if (!valuesWithThisHash.length)
                        this.dic[keyHash] = undefined;

                    this.valuesCount--;
                    return true;
                }
            }

            return false;
        }

        /** Check if this dictionary contains a value at the speficied key. */
        public contains(key: K): boolean {
            var keyHash = this.hash(key);
            var kvp = this.getLowLevel(key, keyHash);
            return !!kvp;
        }

        /** Get the count of values in this dictionary. */
        public count(): number {
            return this.valuesCount;
        }

        /**
         * Convert key to (possibly unique) string, the same string must always
         * produce the same hash.
         */
        protected hash(key: K): string {
            return fg.util.valueToString(key);
        }

        /** Compare two keys for equality. */
        protected areEqual(key1: K, key2: K): boolean {
            // TODO
            return true;
        }

        private getLowLevel(key: K, keyHash: string): { k: K; v: V; } {
            var valuesWithThisHash = this.dic[keyHash];

            if (!valuesWithThisHash)
                return null;

            for (var i = 0; i < valuesWithThisHash.length; i++)
                if (this.areEqual(valuesWithThisHash[i].k, key))
                    return valuesWithThisHash[i];

            return null;
        }
    }
}
