/// <reference path="../core/Event.ts" />

module fg.util {

    /** Countdown before fireing an event. */
    export class Delay {

        /** Event fired when the delay times out. */
        public onDelay = core.event(this);
        private onDelayInstance = () => {
            this.timeout = null;
            this.onDelay.fire();
        }

        private timeout: number;

        constructor(
            /** Countdown time before fireing the event. */
            private time: number = 200
            ) { }

        /** Check wether there is a countdown set or not. */
        public isSet(): boolean {
            return this.timeout != null;
        }

        /** Start the countdown for this delay, if none is already running. */
        public set(): void {
            if (this.isSet())
                return;

            this.timeout = setTimeout(this.onDelayInstance, this.time);
        }

        /** Restart the countdown for this delay, or start a new one. */
        public restart(): void {
            this.abort();
            this.set();
        }

        /** Restart the countdown for this delay only if there is a countdown already running. */
        public reset(): void {
            if (this.isSet())
                this.restart();
        }

        /** Fire the event without running the countdown. */
        public fire(): void {
            this.abort();
            this.onDelayInstance();
        }

        /** Fire immediately if a countdown is running. */
        public haste(): void {
            if (this.isSet())
                this.fire();
        }

        /** Interrupt the countdown, if any. */
        public abort(): void {
            if (!this.isSet())
                return;

            clearTimeout(this.timeout);
            this.timeout = null;
        }
    }
}
