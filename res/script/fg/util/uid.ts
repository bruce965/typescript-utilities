/// <reference path="../core/WebStorage.ts" />

module fg.util {

    function randomString(digits: number) {
        var string = '';

        for (var i = 0; i < digits; i++)
            string += Math.floor(Math.random() * 36).toString(36);

        return string;
    }

    var persistentUid: string = null;
    var transientUid: string = null;

    /** Returns a random globally unique string, length might vary. */
    export function uid(): string {
        if (!persistentUid) {
            persistentUid = core.WebStorage.local.get('__uid_browser_string');

            if (!persistentUid)
                core.WebStorage.local.set('__uid_browser_string', persistentUid = randomString(4));

            if (!transientUid)
                transientUid = randomString(4);
        }

        var timestamp = (new Date().getTime()).toString(36);
        var random = randomString(6);

        return timestamp + persistentUid + transientUid + random;
    }
}
