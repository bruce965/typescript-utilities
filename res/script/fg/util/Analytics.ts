/// <reference path="../../defs/google.analytics/ga.d.ts" />
/// <reference path="../../defs/jquery/jquery.d.ts" />

module fg.util {

    var doNothing = () => {};

    /** Analytics service utilities. */
    export class Analytics {

        private ga: () => UniversalAnalytics.ga;

        constructor(trackingId: string) {
            var debug = location.hostname == 'localhost' || location.hostname == '127.0.0.1';
            if(debug)
                this.ga = <any>(() => doNothing);
            else
                this.initializeGoogleAnalyticsObject();

            this.ga()('create', trackingId, 'auto');
        }

        /** Report an event to analytics server. */
        public event(category: string, action: string, label: string = null, value: number = 1, nonInteraction: boolean = false): void {
            this.ga()('send', {
                hitType: 'event',
                eventCategory: category,
                eventAction: action,
                eventLabel: label,
                eventValue: value,
                nonInteraction: +nonInteraction
            });
        }

        /** Report a page view to analytics server. */
        public pageView(location?: string, page: string = null, title: string = null): void {
            var wlocation = window.location;
            this.ga()('send', {
                hitType: 'pageview',
                location: location || (wlocation.protocol + '//' + wlocation.hostname + wlocation.pathname + wlocation.search),
                page: page,
                title: title
            });
        }

        /** Report an exception to analytics server. */
        public exception(description: string, fatal: boolean = false): void {
            this.ga()('send', {
                hitType: 'exception',
                exDescription: description,
                exFatal: +fatal
            });
        }

        private initializeGoogleAnalyticsObject(): void {
            var gaObject = window['GoogleAnalyticsObject'] = 'ga';
            this.ga = () => window[gaObject];
            if (!window[gaObject]) {
                var ga = window[gaObject] = <any>(function() {
                    window[gaObject].q.push(arguments);
                });
                ga.q = [];
                ga.l = new Date().getTime();
            }

            $(document.createElement('script'))
                .prop('async', <any>true)
                .prop('src', <any>'//www.google-analytics.com/analytics.js')
                .insertBefore($(document.getElementsByTagName('script')[0]));

            return window[gaObject];
        }
    }
}
