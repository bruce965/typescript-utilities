/// <reference path="../../defs/jquery/jquery.d.ts" />
/// <reference path="../dom/make.ts" />

module fg.util.Advertisements {

    /** Advertisements support level. */
    export enum AdsSupport {
        /** Blocked on this page. */
        Blocked = 0,
        /** Allowed on this page. */
        AllowedHere,
        /** Allowed everywhere. */
        AllowedEverywhere
    }

    /** Get the advertisements support level of this browser in this page. */
    export function getSupport(callback: (support: AdsSupport) => void): void {
        if (!document.body) {
            $(document).ready(() => getSupport(callback));
            return;
        }

        var makeDiv = () => {
            return dom.make('div', {
                style: {
                    display: 'block',
                    width: 3,
                    height: 2
                }
            });
        };

        var root = dom.make('div', {
            style: {
                position: 'fixed',
                top: '200%',
                left: '200%',
                pointerEvents: 'none',
                opacity: 0
            }
        }).appendTo($(document.body));

        var testAd = makeDiv().addClass('box-ads');
        root.append(makeDiv(), testAd, makeDiv())

        if (testAd.position().top) {
            root.remove();
            callback(AdsSupport.AllowedEverywhere);
        } else {
            $(document).ready(() => {
                setTimeout(() => {
                    var allowed = testAd.position().top;
                    root.remove();

                    if (allowed)
                        callback(AdsSupport.AllowedHere);
                    else
                        callback(AdsSupport.Blocked);
                });
            });
        }
    };
}
