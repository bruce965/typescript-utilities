/// <reference path="../../defs/jquery/jquery.d.ts" />

module fg.dom {

    /** Describes properties about a DOM element to be created. */
    export interface DomElementProperties {
        clazz?: string[];
        attr?: { [attribute: string]: string };
        prop?: { [property: string]: string|number|boolean };
        style?: { [attribute: string]: string|number };
        text?: string;
        child?: JQuery[];
        on?: { [event: string]: (e: JQueryEventObject) => void };
        data?: { [attribute: string]: any };
    }

    /** Construct a DOM element. */
    export function make(tag: string, args?: DomElementProperties): JQuery {
        var el = $(document.createElement(tag));

        if(!args)
            return el;

        if (args.clazz)
            el.addClass(args.clazz.join(' '));

        if (args.attr)
            for (var k in args.attr)
                el.attr(k, args.attr[k]);

        if (args.prop)
            for (var k in args.prop)
                el.prop(k, args.prop[k]);

        if (args.style)
            el.css(args.style);

        if (args.text)
            el.text(args.text);

        if (args.child)
            for (var i = 0; i < args.child.length; i++)
                el.append(args.child[i]);

        if (args.on)
            for (var k in args.on)
                el.on(k, args.on[k]);

        if (args.data)
            for (var k in args.data)
                el.data(k, args.data[k]);

        return el;
    }
}
