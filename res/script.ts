/// <reference path="script/fg/gui/ContextMenu.ts" />
/// <reference path="script/fg/util/valueToString.ts" />
/// <reference path="script/fg/gui/input.ts" />

var testMenu = new fg.gui.ContextMenu();
testMenu.setData({
    child: [{
        label: "Back",
        action: () => console.log("test: back")
    }, {
            label: "Forward"
        }, {
            label: "Reload",
            action: () => console.log("test: reload")
        }, fg.gui.ContextMenu.separator, {
            label: "Save as...",
            icon: "http://www.famfamfam.com/lab/icons/silk/icons/disk.png",
            child: [{
                label: "Test sub-item 1",
                action: () => console.log("test: save-as item1")
            }, {
                    label: "Test sub-item 2",
                    action: () => console.log("test: save-as item2")
                }, {
                    label: "Test sub-item 3",
                    action: () => console.log("test: save-as item3")
                }]
        }, {
            label: "Print...",
            action: () => console.log("test: print")
        }, {
            label: "Translate to...",
            child: [{
                label: "English",
                action: () => console.log("test: translate to english")
            }, {
                    label: "Italian",
                    action: () => console.log("test: translate to italian")
                }]
        }, {
            label: "View page source",
            action: () => console.log("test: view source")
        }, {
            label: "View page info",
            action: () => console.log("test: view info")
        }, fg.gui.ContextMenu.separator, {
            label: "Inspect element",
            action: () => console.log("test: inspect")
        }]
});

var openMenu = (e: JQueryEventObject) => {
    testMenu.open();
    e.preventDefault();
};

$(() => {
    fg.gui.input.mouse.getButton(fg.gui.input.MouseButton.left).isDown.onValue(true).bind(() => console.log("Left mouse down"));
    fg.gui.input.mouse.getButton(fg.gui.input.MouseButton.left).isDown.onValue(false).bind(() => console.log("Left mouse up"));

    fg.dom.make('div', {
        style: {
            border: '1px solid black',
            width: '100%',
            height: 100
        },
        on: {
            contextmenu: openMenu
        }
    }).prependTo(document.body);
});
